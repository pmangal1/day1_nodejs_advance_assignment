function Calculator() {
  this.numOne = 0;
  this.numTwo = 0;
}

Calculator.prototype = {
  read: function () {
    this.numOne = Number(prompt('Enter First Number'));
    this.numTwo = Number(prompt('Enter Second Number'));
  },

  sum: function () {
    return this.numOne + this.numTwo;
  },

  mul: function () {
    return this.numOne * this.numTwo;
  }
  
}

calc = new Calculator()
console.log(calc)
calc.read()
console.log(calc)
sum = calc.sum()
mul = calc.mul()
console.log(sum, mul)
