// check first letter of string to uppercase
function checkString(str){
  str = str.trim()
  if(str[0] != str[0].toUpperCase()){
    str = str[0].toUpperCase()+str.slice(1)
  }
  
  return str
}

console.log(checkString(' prakhar'))
