function deepClone(originalObject) {
  let clonedObject = Object.create(originalObject.__proto__)

  function helper(obj, newObj) {
    Object.keys(obj).map(key => {
      if (typeof obj[key] == 'object') {
        newObj[key] = {}
        helper(obj[key], newObj[key])
      } else {
        newObj[key] = obj[key];
      }

    })

  }
  
  helper(originalObject, clonedObject)
  return clonedObject
}

originalObject = {
  name: 'prakhar',
  age: '21',
  company: 'deqode',
  k1: {
    k2: {
      k3: 1
    }
  },
}

copy = deepClone(originalObject)
console.log(copy)
