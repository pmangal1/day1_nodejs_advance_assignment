function factorial() {
  let memory = []
  memory[0] = memory[1] = 1

  return function (number) {
    if(memory.length-1 >= number){
      return memory[number]
    }else{
      for(let i=memory.length;i<=number;i++){
        memory[i] = memory[i-1]*i 
      }

      return memory[number]
    }

  }

}

let calculateFactorial = factorial()
console.log(calculateFactorial(3))
console.log(calculateFactorial(6))
